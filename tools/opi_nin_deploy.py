import argparse
import logging
from pathlib import Path
from random import choice
from string import ascii_lowercase
from typing import List, Tuple

import git
from git import Repo


def write_commit_msg(changed_files: List[Tuple[str, str]]) -> str:
    sub_update_str: str = "Updated submodule(s):\n"
    sub_add_str: str = "Added submodule(s):\n"
    others_update_str: str = "Updated file(s):\n"
    others_add_str: str = "Added file(s):\n"

    for file_name, action in changed_files:
        if "moduleDisplays/" in file_name:
            if action == "Update":
                sub_update_str += f"- {file_name.split('moduleDisplays/', 1)[1]}\n"
            elif action == "Add":
                sub_add_str += f"- {file_name.split('moduleDisplays/', 1)[1]}\n"
        else:
            if action == "Update":
                others_update_str += f"- {file_name}\n"
            elif action == "Add":
                others_add_str += f"- {file_name}\n"

    return "Updates\n\n{}{}{}{}".format(
        sub_update_str, sub_add_str, others_update_str, others_add_str
    )


def repository_changes(repository: Repo) -> List[Tuple[str, str]]:
    files = []

    for item in repository.index.diff(None):
        files.append((item.a_path, "Update"))

    for file in repository.untracked_files:
        files.append((file, "Add"))

    for item in repository.index.diff("HEAD"):
        action_str = "Add" if item.change_type == "D" else "Update"
        files.append((item.a_path, action_str))

    return files


def remove_path(path: Path):
    if path.is_file() or path.is_symlink():
        path.unlink()
        return
    for p in path.iterdir():
        remove_path(p)
    path.rmdir()


def apply_actions(
    repo: Repo, commit_msg: str, dry_run: bool, create_merge_request: bool
) -> None:

    push_cmd = ["--set-upstream", "origin", "main"]
    repo_url = repo.remotes.origin.url.split(":")[1]

    if dry_run:
        logging.debug(f"The changes: \n{commit_msg}\nwould apply")
    else:
        if create_merge_request:
            logging.debug("Creating Merge Request to main branch")
            branch_name = "generated_branch_" + "".join(
                choice(ascii_lowercase) for _ in range(16)
            )
            new_branch = repo.create_head(branch_name)
            new_branch.checkout()
            push_cmd = push_cmd[:-1] + [branch_name, "-o", "merge_request.create"]

        repo.git.add(all=True)
        repo.index.commit(commit_msg)
        repo.git.push(push_cmd)

        print(f"Pushed changes in the {repo_url} repository")


def deploy_nin_opis(
    nss_repo: str,
    dry_run: bool,
    nin_folder: str,
    no_merge_request: bool,
    debug: bool,
):
    if debug:
        logging.basicConfig(level=logging.DEBUG)

    create_merge_request = not no_merge_request
    main_folder = Path().resolve().parent

    # Update submodules with remote
    logging.debug("Updating OPIs submodules")
    main_repo = git.Repo(main_folder)
    main_repo.git.submodule("update", "--remote")

    # check if there are changes in the main repo or submodules
    repo_changed = main_repo.is_dirty(untracked_files=True)

    if repo_changed:
        logging.debug("Fetching repository changes")
        files = repository_changes(main_repo)
        commit_msg = write_commit_msg(files)

        apply_actions(main_repo, commit_msg, dry_run, False)

        # Remove clone destination path if exists.
        if Path(nin_folder).is_dir():
            remove_path(Path(nin_folder))

        logging.debug("Cloning NSS repository")
        nin_repo = git.Repo.clone_from(nss_repo, nin_folder)
        nin_repo.git.submodule("update", "--init", "--remote")

        logging.debug("Fetching repository changes")
        files = repository_changes(nin_repo)
        commit_msg = write_commit_msg(files)

        apply_actions(nin_repo, commit_msg, dry_run, create_merge_request)


def main():
    """Run the main function."""
    parser = argparse.ArgumentParser(
        "Utility for update the opisControlDashboard and deploy it in NIN"
    )

    parser.add_argument(
        "-r",
        "--nss-repo",
        default="git@gitlab.esss.lu.se:opis/nss/ymir/engineer.git",
        help="NSS Instrument OPI repository",
    )

    parser.add_argument(
        "-n", "--dry-run", action="store_true", help="Don't actually commit anything"
    )

    parser.add_argument(
        "-f",
        "--nin-folder",
        default="/tmp/opi_nin",
        help="OPI NIN repository destination clone folder",
    )

    parser.add_argument(
        "--no_merge_request",
        action="store_true",
        help="Skip creating a merge request, commit directly to the default branch (Dangerous!!!)",
    )

    parser.add_argument("--debug", action="store_true", help="Enable debugging trace")

    deploy_nin_opis(**vars(parser.parse_args()))


if __name__ == "__main__":
    main()
